package start;
import java.sql.SQLException;
import java.util.logging.Logger;

import bll.ClientBLL;
import bll.OrdersBLL;
import bll.ProductBLL;
import bll.StockBLL;
import presentation.Controller;
import presentation.View;

/**
 * Created by Rosca on 10.04.2017.
 */
public class Start {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

    public static void main(String[] args) throws SQLException {

        ClientBLL clientBll = new ClientBLL();
        ProductBLL productBLL = new ProductBLL();
        OrdersBLL ordersBLL = new OrdersBLL();
        StockBLL stockBLL = new StockBLL();
        View view = new View();

        Controller controller = new Controller(view, clientBll,productBLL,stockBLL,ordersBLL);


       // clientBll.insert1();
        //clientBll.update1();




    }

}