package model;

/**
 * Created by Rosca on 12.04.2017.
 */
public class Product {
    private int id;
    private String name;
    private int price;

    public Product(){}

    public Product(int id, String name, int price){
        super();
        this.id  = id;
        this.name = name;
        this.price = price;
    }

    public Product(String name, int price){
        super();
        this.name = name;
        this.price = price;
    }


    public int getId() {
        return id;
    }

    public void setId( int id){
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Product [ id= " + this.id + " name= "+ this.name +   " price = "  + this.price + "]\n";
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
