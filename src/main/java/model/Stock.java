package model;

/**
 * Created by Rosca on 12.04.2017.
 */
public class Stock {
    private int id;
    private int product;
    private int quantityProduct;

    public Stock(){}

    public Stock(int id, int product, int quantityProduct ){
        super();
        this.id = id;
        this.product = product;
        this.quantityProduct = quantityProduct;
    }

    public Stock(int product, int quantityProduct ){
        super();
        this.product = product;
        this.quantityProduct = quantityProduct;
    }

    @Override
    public String toString() {
        return "Stock [ product = " + product + " quantityProduct= "+ quantityProduct + " ]\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getQuantityProduct() {
        return quantityProduct;
    }

    public void setQuantityProduct(int quantityProduct) {
        this.quantityProduct = quantityProduct;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }
}
