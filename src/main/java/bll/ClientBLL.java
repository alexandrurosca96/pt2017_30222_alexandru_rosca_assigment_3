package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.ClientAgeValidator;
import bll.validators.Validator;
import dao.ClientDAO;
import model.Client;

import javax.swing.*;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems
 *          Research Laboratory, http://dsrl.coned.utcluj.ro/
 * @Since: Apr 03, 2017
 */
public class ClientBLL {

    private List<Validator<Client>> validators;
    private ClientDAO clientDAO;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
        validators.add(new ClientAgeValidator());

        clientDAO = new ClientDAO();
    }

    public Client findClientById(int id) {
        Client st = clientDAO.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The Client with id =" + id + " was not found!");
        }
        return st;
    }

    public List<Client> findAllClients(){
        List<Client> clients = new ArrayList<Client>();

        clients = clientDAO.findAll();

        if(clients == null){
            throw new NoSuchElementException("There are no Clients!");
        }
        return clients;

    }

    public void insertClient(Client client){
        try {
            for(Validator<Client> validator: validators){
                validator.validate(client);
            }
            clientDAO.insert(client);
        }catch(Exception e){
            throw new NoSuchElementException("Can't insert client!\n" + e.getMessage());
        }
    }

    public void deleteClientById(int id){
        try{
            clientDAO.deletebyId(id);
        }catch(Exception e1){
            throw new NoSuchElementException("Can't delete Client!");
        }
    }

    public void updateClient(Client client){
        try{
            clientDAO.update(client, client.getId());
        }catch(Exception e2){
            throw new NoSuchElementException("Can't update client!");
        }
    }

    public JTable createTable(List<Client> list){
        JTable table;
        try {
            table = clientDAO.createTable(list);
        }catch(Exception e3){
            throw new NoSuchElementException("Can't create table!");
        }
        return table;
    }

}