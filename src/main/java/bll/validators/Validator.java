package bll.validators;

/**
 * Created by Rosca on 10.04.2017.
 */
public interface Validator<T> {

    public void validate(T t);
}