package bll.validators;

import model.Product;

import java.util.NoSuchElementException;

/**
 * Created by Rosca on 21.04.2017.
 */
public class ProductPriceValidator implements  Validator<Product>{
    public void validate(Product product) {
        if(product.getPrice()  < 0 ){
            throw new IllegalArgumentException("Price can't be negative!");
        }
    }
}
